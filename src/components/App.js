import React, { Component } from 'react';
import movies$ from '../data/movies'
import store from '../store/index'
import './App.css';
import Card from './card'
import Filter from './filter'
import { shouldCardShow, getValues } from '../lib/index'
import { connect } from "react-redux";
import { fetchProducts, changeDisplay, changeCategory, deleteMovie } from "../store/actions/index";

class ConnectApp extends Component {

  componentDidMount() {
    this.props.getData()
  }

  render() {
    const { 
      movies, 
      category, 
      displayNb, 
      handleChange,
      handleDelete, 
      handleDisplay 
    } = this.props
    let categories = getValues(movies,"category")

    return (
     <div className = "card-container">
       <div style = {{width:"100%"}}>
         <Filter 
            values = {categories}
            selected = {category}
            handleChange = {handleChange}
          /> 
          <Filter 
            values = {[4,8,12]}
            selected = {displayNb}
            handleChange = {handleDisplay}
          />
       </div>

      {movies.map((movie,index) => 
        shouldCardShow(category, movie, index, displayNb)
        && <Card 
          key = {index+movie}
          content = {movies[index]}
          onDelete = {handleDelete}
        />
      )}
     </div>
    );
  }
}

const mapStateToProps = state => ({
  movies: state.movies,
  status: state.status,
  displayNb: state.displayNb,
  category: state.category
})

const mapDispatchToProps = dispatch =>({
  getData: () => dispatch(fetchProducts()),
  handleDisplay: event => dispatch(changeDisplay(Number(event.target.value))),
  handleChange: event => dispatch(changeCategory(event.target.value)),
  handleDelete: event => dispatch(deleteMovie(event.target.value))
})


const App = connect(mapStateToProps, mapDispatchToProps)(ConnectApp)
export default App;
