import React,{ Component } from '.react'

export default function Likes (props){
    const { dislike, nb } = props
    const style = dislike ? "thumb dislikes" : "thumb"
    
    return  <p className = {style}>{nb}</p>
}