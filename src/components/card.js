import React, { Component } from 'react'
import './card.scss'
import PropTypes from 'prop-types';
import { toggleLike } from '../store/actions';
import { connect } from 'react-redux'

function Card({ content, onDelete, updateLikes }) {

    const {
        title = "",
        category = "",
        likes = "",
        dislikes = "",
        id = "",
    } = content
    return (
        <div className="custom-card">
            <p className="italic">{category}</p>
            <div className="inline">
                <Likes dislike={false} nb={likes} />
                <Likes dislike={true} nb={dislikes} />
            </div>
            <CustomButton
                handleClick={onDelete}
                value={id}
                content="Delete" />
            <LikeButton
                like = {likes}
                id = {id}
                //handleClick = {updateLikes}
            />
            <h4>{title}</h4>
        </div>
    )
}
export default Card

Card.propTypes = {
    content: PropTypes.object,
    onDelete: PropTypes.func,
    upDateLikes: PropTypes.func
}

class ConnectLikeButton extends Component{
    state = {
        likeStatus: false
    }
    handleClick = (ev) => {
        this.setState(state => ({ 
            likeStatus: !state.likeStatus 
        }),
        this.props.click(ev,this.state.likeStatus))
    }
    render(){
        const { like, id } = this.props
        const { likeStatus } = this.state
        var content = likeStatus ? "Unlike" : "I like it"
        return (
            <button
                className = "toggleButton"
                onClick = {this.handleClick}
                value = {id}>
                {content}
            </button>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
      click: (event,likeStatus) => {
        dispatch(toggleLike(event.target.value,likeStatus))
      }
    }
}
const LikeButton = connect(null,mapDispatchToProps)(ConnectLikeButton)


function Likes({ dislike, nb }) {

    const style = dislike ? "thumb dislikes" : "thumb"
    return <p className={style}>{nb}</p>
}

Likes.propTypes = {
    dislike: PropTypes.bool,
    nb: PropTypes.number
}

function CustomButton({ handleClick, value, content }) {
    return (
        <button onClick = {handleClick}
            value = {value}
            className = "custom-btn"
        >
            {content}
        </button>
    )
}

CustomButton.propTypes = {
    handleClick: PropTypes.func,
    value: PropTypes.string,
    content: PropTypes.string
}