export function shouldCardShow(category, movie, index, displayNb){
  return (( category === movie.category 
    || category === "All")
    && index < displayNb)
}

export function getValues(object,key){
  let categories = ["All"]
  if(object)
    object.forEach((obj) => {
      if(!categories.includes(obj[key])){
        categories.push(obj[key])
      }
    }) 
  
  return categories
}