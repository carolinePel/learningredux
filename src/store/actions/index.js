import { 
    CHANGE_DISPLAY,
    CHANGE_CATEGORY,
    FETCH_PRODUCTS_BEGIN,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    TOGGLE_LIKE,
    DELETE_MOVIE
} from '../constants/actions-type'

//action creators to be used in dispatch method

export const deleteMovie = (id) => ({
    type: DELETE_MOVIE,
    payload: id
})

export const toggleLike = (id,likeStatus) => ({
    type: TOGGLE_LIKE, 
    payload: { id, likeStatus }
})

export const changeDisplay = payload => ({ 
    type: CHANGE_DISPLAY, payload 
})

export const changeCategory = payload => ({
    type: CHANGE_CATEGORY, payload
})

export const fetchProductsBegin = () => ({
    type: FETCH_PRODUCTS_BEGIN
})
  
export const fetchProductsSuccess = movies => ({
    type: FETCH_PRODUCTS_SUCCESS,
    payload: { movies:JSON.parse(movies) }
})

export const fetchProductsFailure = error => ({
    type: FETCH_PRODUCTS_FAILURE,
    payload: { error }
})

export function fetchProducts() {
    return dispatch => {
      dispatch(fetchProductsBegin());
      return fetch('../../data/movies.json')
        .then(res => {
            return res.json()
        })
        .then(json => {
          //console.log("dispatch", json)
          dispatch(fetchProductsSuccess(json))
          return json
        })
        .catch(error => {
            console.log("error", error)
            return dispatch(fetchProductsFailure(error))
        });
    };
  }