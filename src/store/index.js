import { createStore, applyMiddleware } from "redux"
import AppReducer from "./reducer/index"
import thunk from "redux-thunk"
import { changeDisplay, changeCategory, fetchProductsBegin, fetchProductsSuccess, toggleLike, fetchProductsFailure, deleteMovie } from "./actions";

const store = createStore(AppReducer, applyMiddleware(thunk))

/**
 * Test the store
 */
console.log("initial state:", store.getState())
const unsubscribe = store.subscribe(() => console.log(store.getState()))
/*
store.dispatch(changeDisplay(4))
store.dispatch(changeCategory("Comedy"))
store.dispatch(fetchProductsBegin())
store.dispatch(fetchProductsSuccess("[{\"id\":\"0\",\"title\":\"Oceans 8\",\"category\":\"Comedy\",\"likes\":4,\"dislikes\":1},{\"id\":\"2\",\"title\":\"Midnight Sun\",\"category\":\"Comedy\",\"likes\":2,\"dislikes\":0}]"))
store.dispatch(toggleLike(0,false))
console.log("delete")
store.dispatch(deleteMovie("2"))
store.dispatch(fetchProductsFailure("erreur"))
*/
unsubscribe()

export default store