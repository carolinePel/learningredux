//import CHANGE_DISPLAY from '../constants/actions-type'
import { 
    CHANGE_DISPLAY, 
    CHANGE_CATEGORY,
    FETCH_PRODUCTS_BEGIN, 
    FETCH_PRODUCTS_FAILURE, 
    FETCH_PRODUCTS_SUCCESS,
    TOGGLE_LIKE,
    DELETE_MOVIE 
} from "../constants/actions-type";

const initialState = {
    category: "All",
    displayNb: 8,
    movies: [],
    status:{
        loading: false,
        error: null
    }
}

function categoryReducer(state = initialState.category, action){
    if(action.type === CHANGE_CATEGORY){
        return action.payload
    }
    return state
}
function moviesReducer(state = initialState.movies, action){
    if(action.type === TOGGLE_LIKE){
        const { likeStatus, id } = action.payload
        
        return state.map((movie,index) => {
            if((Number(movie.id) === Number(id)) && likeStatus === true) {
                return {
                    ...movie,
                    likes: movie.likes - 1,
                }
            }
            else if((Number(movie.id) === Number(id)) && likeStatus === false){
                return {
                    ...movie,
                    likes: movie.likes + 1,
                }
            }
            else{
                return movie
            }
        
        })
    }
    if(action.type === FETCH_PRODUCTS_SUCCESS){
        return action.payload.movies
    }
    if(action.type === FETCH_PRODUCTS_FAILURE){
        return []
    }
    if(action.type === DELETE_MOVIE){
        return state.filter(movie => Number(movie.id) !== Number(action.payload))
    }
    return state
}

function displayReducer(state = initialState.displayNb, action){
    if(action.type === CHANGE_DISPLAY)
    {
        return action.payload
    }
    return state
}

function statusReducer( state = initialState.status, action){

    if(action.type === FETCH_PRODUCTS_BEGIN)
    {
        return {
            loading: true,
            error: null
        }
    }
    if(action.type === FETCH_PRODUCTS_SUCCESS)
    {
        return{
            loading: false,
            error: null
        }
    }
    if(action.type === FETCH_PRODUCTS_FAILURE)
    {
        return {
            loading: false,
            error: action.payload.error,
        }
    }
    return state
}

function AppReducer(state = initialState, action){
    return {
        category: categoryReducer(state.category, action),
        displayNb: displayReducer(state.displayNb,action),
        movies: moviesReducer(state.movies,action),
        status: statusReducer(state.status,action)
    }
}

export default AppReducer